# MAC0110 - MiniEP7
# Victor Aristóteles Rocha Campos - 11796913


function sin(x) # Função seno
    #Primeiro, criaremos um array de números ímpares, para alternarmos o sinal da série de taylor conseguinte de acordo com a iteração
    arrayImpares = fill(0,15)
    j = 1
    for i in 1:15
        arrayImpares[i] = j
        i += 1
        j += 2
    end

    # Agora para as séries de Taylor    
    if (x > pi)
        return "Valor de x inválido! Manter valores menores ou iguais a de π"
    elseif (x <= pi)
        valor = 0
        for i in 1:10
            if isodd(i) 
                valor = valor + ((x^arrayImpares[i]) / factorial(arrayImpares[i]))
                
            elseif iseven(i)
                valor = valor - ((x^arrayImpares[i]) / factorial(arrayImpares[i]))
                
            end
        i += 1
        end
        return valor
    end
end

function cos(x) # Função cosseno
    arrayPares = fill(0,15)
    j = 0
    for i in 1:15
        arrayPares[i] = j
        i += 1
        j += 2
    end
    
    if (x > pi)
        return "Valor de x inválido! Manter valores menores ou iguais a π"
    elseif (x <= pi)
        valor = 0
        for i in 1:10
           if isodd(i)
                valor = valor + ((x^arrayPares[i]) / factorial(arrayPares[i]))
                
           elseif iseven(i)
                valor = valor - ((x^arrayPares[i]) / factorial(arrayPares[i]))
                
           end
        i += 1
        end
        return valor
    end
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

# Para tan(x), o valor válido de x proposto pelo exercício é |x| < pi/2
function tan(x)
    x = abs(x)
    divisao = sin(x) / cos(x)
    if x >= (pi/2)
        return "Valor inválido! Valor restrito à (x <= π/2) "
    elseif x < (pi/2)
        res = 0
        for i in 1:10
            res = divisao +  (2^(2*i) * (2^(2*i) - 1) * (bernoulli(i) * (x^((2*i) - 1))) / (factorial(2*i)))
            i += 1
        end
    return res
    end
end

# Para as funções check, usamos uma tolerância de erro de +- 0.001
function check_sin(value, x)
    if ((taylor_sin(x) - value) < 0.001 ) || ((taylor_sin(x) - value) < (-0.001))
        return true
    else
        return false
    end
end

function check_cos(value, x)
    if ((taylor_cos(x) - value) < 0.001 ) || ((taylor_cos(x) - value) < (-0.001))
        return true
    else
        return false
    end
end

function check_tan(value, x)
    println("Lembre-se, o valor de tan(x) é restrito a |x| < π/2 pelo enunciado")
    if ((taylor_tan(x) - value) < 0.001 ) || ((taylor_tan(x) - value) < (-0.001))
        return true
    else
        return false
    end
end

function test()
    # TESTES PARA FUNÇÕES DA PARTE 1
    if abs(taylor_sin(π/6) - 1/2) > 0.001   || abs(taylor_cos(π/6) - sqrt(3)/2) > 0.001 || abs(taylor_tan(π/6) - sqrt(3)/3) > 0.001 || abs(taylor_sin(π/3) - sqrt(3)/2) > 0.001 || abs(taylor_cos(π/3) - 1/2) > 0.001 || abs(taylor_tan(π/3) - sqrt(3)) > 0.001 || abs(taylor_sin(π) - 0) > 0.001 || abs(taylor_cos(π) - (-1)) > 0.001  
        return "Há um erro nas funções da parte 1."
    #TESTES PARA FUNÇÕES DA PT2
    elseif (check_sin(1/2,pi/6) != true) || (check_cos(1/2,pi/3) != true) || (check_tan(1,pi/4) != true)
        return "Há um erro nas funções da parte 2."
    end
    return "Tudo OK :)"
end

test()

function taylor_sin(x) # Função seno
    #Primeiro, criaremos um array de números ímpares, para alternarmos o sinal da série de taylor conseguinte de acordo com a iteração
    arrayImpares = fill(0,15)
    j = 1
    for i in 1:15
        arrayImpares[i] = j
        i += 1
        j += 2
    end

    # Agora para as séries de Taylor    
    if (x > pi)
        return "Valor de x inválido! Manter valores menores ou iguais a de π"
    elseif (x <= pi)
        valor = 0
        for i in 1:10
            if isodd(i) 
                valor = valor + ((x^arrayImpares[i]) / factorial(arrayImpares[i]))
                
            elseif iseven(i)
                valor = valor - ((x^arrayImpares[i]) / factorial(arrayImpares[i]))
                
            end
        i += 1
        end
        return valor
    end
end

function taylor_cos(x) # Função cosseno
    arrayPares = fill(0,15)
    j = 0
    for i in 1:15
        arrayPares[i] = j
        i += 1
        j += 2
    end
    
    if (x > pi)
        return "Valor de x inválido! Manter valores menores ou iguais a π"
    elseif (x <= pi)
        valor = 0
        for i in 1:10  
            if isodd(i)
                valor = valor + ((x^arrayPares[i]) / factorial(arrayPares[i]))
            elseif iseven(i)
                valor = valor - ((x^arrayPares[i]) / factorial(arrayPares[i]))
                
            end
        i += 1
        end
        return valor
    end
end

# Para tan(x), o valor válido de x proposto pelo exercício é |x| < pi/2
function taylor_tan(x)
    x = abs(x)
    divisao = sin(x) / cos(x)
    if x >= (pi/2)
        return "Valor inválido! Valor restrito à (x <= π/2) "
    elseif x < (pi/2)
        res = 0
        for i in 1:10
            res = divisao +  (2^(2*i) * (2^(2*i) - 1) * (bernoulli(i) * (x^((2*i) - 1))) / (factorial(2*i)))
            i += 1
        end
    return res
    end
end
