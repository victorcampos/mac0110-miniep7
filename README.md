# MAC0110-MiniEP7

MiniEP7 coursework for the MAC0110 discipline, found in the trigonometria.jl file.

Exercise 2.2 consists of four functions. sin(x) and cos(x) are used to calculate the sine
and cosine of a given number x (in radians), employing the Taylor series to do so. 
Meanwhile, tan(x) will similarly use the Taylor series to calculate the tangent of a given x,
but will sum that to another value, the latter of which is calculated using the Bernoulli number,
given by our fourth function bernoulli(n).

Exercise 3.1 consists of three more functions, built to check the functions coded in part 1 whilst taking
into account the pontential computer errors and compensating accordingly.

Exercise 4.1 consists of automated tests utilizing pre-computated values for all seven aforementioned functions,
returning values on whether errors were made or if all calculations return their expected values.

Exercise 4.2, found at the end of the code, consist of the same functions from part 1 but renamed according to
the Taylor Series that they use to compute their respective results - Also used for not getting mixed up with
Julia's native trigonometric relation functions.

USAGE: Execute trigonometria.jl to run automated tests with preset values.
Alternatively, you may call each individual function with values of your choosing to be calculated, with each of them having a specific set of valid input values.
